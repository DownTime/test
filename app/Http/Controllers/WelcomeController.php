<?php

namespace App\Http\Controllers;
use DB;


class WelcomeController extends Controller
{
    public function welcome_laravel(){
        $status = 200;
        $param = [
            'success' => "test",
            'message' => "test",
            'token' => "test",
            'data' => "test",
            'errors' => "test",
        ];
        return response()->json($param, $status);
    }
    public function testdatabase(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $data = DB::select("SELECT * FROM user");
            $report = array('data' => $data, 'status' => $status);
        }else{
            $status = 401;
            $data = [];
            $report = array('data' => $data, 'status' => $status);
        }
        return response()->json($report);
    }
}
