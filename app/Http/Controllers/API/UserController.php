<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


class UserController extends Controller
{


    public $successStatus = 200;


    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['name' => request('name'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $user_id = Auth::user()->name;
            $success['user'] = $user_id;
            $success['user_status'] = null;
            $success['emp_id'] = null;
            $success['success'] = false;

            $data = DB::select("SELECT * FROM `user` WHERE `user_id` LIKE '$user_id' ORDER BY `user_status` ASC");
            foreach ($data as $key => $value ){
                $success['user_status'] = $data[0]->user_status;
                $success['emp_id'] = $data[0]->emp_id;
                $success['success'] = true;
            }
            $emp_id = $success['emp_id'];
            $data2 = DB::select("SELECT `emp_data` FROM `employee` WHERE `emp_id` = '$emp_id'");
            if (count($data2) > 0) {
                $success['emp_data'] = $data2[0]->emp_data;
            }else {
                $success['emp_data'] = null;
            }

            $pass = DB::select("SELECT `user_id` FROM `user` WHERE `user_pass` IS NULL AND `user_id` = '$user_id'");
            if (count($pass) > 0) {
                $success['change_pass'] = false;
            }else {
                $success['change_pass'] = true;
                $success['user_status'] = 5;
            }

            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            // return response()->json(['error'=>'Unauthorised'], 401);
            $success['success'] = false;
            return response()->json(['success' => $success], 200);
        }
    }

    public function logout(){
        Auth::user()->token()->revoke();
        $data = Auth::user()->token()->delete();
        return response()->json(['success'=>$data]);
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'email' => 'required|email',
            'password' => 'required',
            // 'c_password' => 'required|same:password',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }


        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;


        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required',
            'level' => 'required',
            'nickname' => 'required',
            'position' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all();
    
        if ($input['password'] == 'random') {
            function RandomString()
            {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                $randstring = $characters[rand(0, strlen($characters)-1)];
                return $randstring;
            }
            $password = '';
            $rand = RandomString();
            for ($i = 0; $i < 8; $i++){
                $rand = RandomString();
                $password .= $rand;
            }
            $input['password'] = $password;
        }
        $success['pass'] =  $input['password'];
        function max_empID(){
            $emp_id = DB::select("SELECT MAX(emp_id) FROM `employee`");
            foreach ($emp_id as $key => $value){
                foreach ($value as $key2 => $value2){
                    $max_empID = $value2;
                }
            }
            $emp_id2 = $max_empID + 1;
            return $emp_id2;
        }
        $user_id = $input['name'];
        $data = DB::select("SELECT count(`user_id`) FROM `user` WHERE `user_id` LIKE '$user_id'");
        $count = null;
        foreach ($data as $key => $value){
            foreach ($value as $key2 => $value2){
                // print_r ($value2);
                $count = $value2;
            }
        }
        if ($count == 0){
            $emp_id = max_empID();
            DB::table('employee')->insert([
                ['emp_id' => $emp_id, 
                'EnNo' => null, 
                'emp_sex' => null,
                'emp_name' => null, 
                'emp_lastname' => null,
                'emp_nickname' => $input['nickname'],
                'emp_position' => $input['position'],
                'emp_address' => null,
                'emp_birthday' => null,
                'emp_weight' => null,
                'emp_height' => null,
                'emp_img' => null,
                'emp_routine' => 1,
                'emp_still' => 1,
                'emp_data' => 0,]
            ]);
            DB::table('user')->insert([
                [   'user_id' => $input['name'], 
                    'user_pass' => $input['password'], 
                    'user_status' => $input['level'],
                    'emp_id' => $emp_id,
                ]
            ]);
            DB::table('star')->insert([
                [   'star_id' => null, 
                    'star_1' => null, 
                    'star_2' => null,
                    'star_3' => null,
                    'star_4' => null,
                    'star_5' => null,
                    'emp_id' => $emp_id,
                ]
            ]);
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            $success['status'] = true;
            return response()->json(['success'=>$success], $this->successStatus);
        }else {
            $success['pass'] = null;
            $success['token'] = null;
            $success['name'] =  '่เบอร์โทรศัพศ์ซ้ำ';
            $success['status'] = false;
            return response()->json(['success'=>$success], $this->successStatus);
        }
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function AddPositionsApi(){
        $_POST = json_decode(file_get_contents('php://input'),true);
        $position = $_POST["position"];
        $user = Auth::user();
        $number = $user->name;
        $SQL = DB::table('user')
            ->select('user_status')
            ->where('user.user_id', '=', $number)
            ->get();
        $SQL = $SQL[0]->user_status;
        if ($SQL > 2){
            $status = 'สิทธ์ถูกต้อง';
            DB::table('position')->insert([
                ['pos_id' => null,
                'pos_name' => $position]
            ]);
        }else {
            $status = 'สิทธ์ไม่ถูกต้อง';
        }
        $report = array('status' => $status, 'user' => $user, 'data' => $SQL, 'tset' => $position);
        
        return response()->json($report);
    }

    public function DeletePositionsApi(){
        $_POST = json_decode(file_get_contents('php://input'),true);
        $position = $_POST["position"];
        $user = Auth::user();
        $number = $user->name;
        $SQL = DB::table('user')
            ->select('user_status')
            ->where('user.user_id', '=', $number)
            ->get();
        $SQL = $SQL[0]->user_status;
        if ($SQL > 2){
            $status = 'สิทธ์ถูกต้อง';
            DB::delete("DELETE FROM `position` WHERE `position`.`pos_id` = $position");
        }else {
            $status = 'สิทธ์ไม่ถูกต้อง';
        }
        $report = array('status' => $status, 'user' => $user, 'position' => $position);
        
        return response()->json($report);
    }

    public function newPassApi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'conpassword' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $user = Auth::user();
        // $number = $user->name;

        $input = $request->all();
        $success['password'] =  $input['password'];
        $success['conpassword'] =  $input['conpassword'];
        $success['user'] = $user;
        $success['number'] = $user->name;
        $success['password'] = bcrypt($input['password']);
        // $user->email = 'john@foo.com';
        $user->password = $success['password'];
        $user->save();
        DB::table('user')->where('user_id', $user->name)->update(['user_pass' => null]);

        return response()->json(['success' => $success, 'teststatus' => true]);
    }

}