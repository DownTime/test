<?php

namespace App\Http\Controllers\upload;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class UploadController extends Controller
{
    public function UploadFile () {
        $_POST = json_decode(file_get_contents('php://input'),true);
        $fd = $_POST['fd'];
        // print_r ($fd);
        $status = "200";
        $report = array('message' => 'UploadFile สำเร็จ (UploadFile)', 'status' => $status , '$fd' => $fd);
        return response()->json($report);
    }
}
