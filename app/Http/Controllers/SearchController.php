<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function user(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $data = DB::select("SELECT * FROM `user` WHERE `user_status` = '1' OR `user_status` = '2' ORDER BY `emp_id`");
            $report = array('data' => $data, 'status' => $status);
        }else{
            $status = 401;
            $data = [];
            $report = array('data' => $data, 'status' => $status);
        }
        return response()->json($report);
    }

    public function manager(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $data = DB::select("SELECT * FROM `user` WHERE `user_status` = '3' ORDER BY `emp_id`");
            $report = array('data' => $data, 'status' => $status);
        }else{
            $status = 401;
            $data = [];
            $report = array('data' => $data, 'status' => $status);
        }
        return response()->json($report);
    }

    public function alluser(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            // $user = DB::select("SELECT * FROM `user` WHERE `user_status` = 1 OR `user_status` = 2 ORDER BY `emp_id` ASC");
            $user = DB::table('user')
            ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
            ->select('employee.*', 'user.*')
            ->where('user.user_status', '<=', 2)
            ->orderBy('user.emp_id', 'ASC')
            ->get();

            // $position = DB::table('position')
            // ->where('position.pos_id', '=', 2)
            // ->get();
            $manager = [];
            foreach ($user as $key => $value) {
                $user_status = $user[$key]->user_status;
                $emp_nickname = $user[$key]->emp_nickname;
                $emp_name = $user[$key]->emp_name;
                $emp_lastname = $user[$key]->emp_lastname;
                $user[$key]->emp_name = "$emp_name";
                
                $position = DB::table('position')
                ->where('position.pos_id', '=', $user[$key]->emp_position)
                ->get();
                
                $position_name = $position[0]->pos_name;
                $user[$key]->emp_position = $position_name;
                
                switch ($user_status) {
                    case 1 :
                        $user[$key]->user_status = "Employee";
                        break;
                    case 2 :
                        $user[$key]->user_status = "Supervisor";
                        break;
                    case 3 :
                        $user[$key]->user_status = "Manager";
                        break;
                    case 4 :
                        $user[$key]->user_status = "Admin";
                        break;
                    default:
                        $user[$key]->user_status = "notuser";
                        break;
                }
                $emp_still = $user[$key]->emp_still;
                switch ($emp_still) {
                    case 0 :
                        $user[$key]->emp_still = "ไม่ทำงานแล้ว";
                        break;
                    case 1 :
                        $user[$key]->emp_still = "ยังทำงานอยู่";
                        break;
                    default:
                        $user[$key]->emp_still = "error";
                        break;
                }
            }
//            print_r ($data); $position[0]->pos_name
            $report = array('user' => $user, 'manager' => $manager, 'status' => $status);
        }else{
            $status = 401;
            $manager = [];
            $user = [];
            $report = array('user' => $user, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }

    public function all_register(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $users = DB::table('employee')
            ->leftJoin('user', 'user.emp_id', '=', 'employee.emp_id')
            ->leftJoin('star', 'user.emp_id', '=', 'star.emp_id')
            ->leftJoin('position', 'employee.emp_position', '=', 'position.pos_id')
            ->select('employee.*', 'star.*', 'position.*', 'user.user_status', 'user.user_id')
            ->where('user.user_status', '<=', 2)
            ->get();
        
            $manager = "อ่านข้อมูลสำเร็จ";
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }else{
            $status = 401;
            $manager = [];
            $users = [];
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }

    public function oneuser(){
        if(DB::connection()->getDatabaseName())
        {
            $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $status = 200;
                $emp_id = $_POST['emp_id'];
                $users = DB::table('user')
                ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
                ->leftJoin('star', 'user.emp_id', '=', 'star.emp_id')
                ->select('employee.*', 'star.*', 'user.user_status', 'user.user_id')
                ->where('user.emp_id', '=', $emp_id)
                ->get();
        
                
                $manager = "อ่านข้อมูลสำเร็จ";
                $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
            }else {
                $status = 401;
                $manager = "ไม่ได้รับค่า emp_id";
                $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
            }
        }else{
            $status = 401;
            $manager = [];
            $users = [];
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }

    public function positions (){
        $status = 200;
        $manager = 'อ่านข้อมูลสำเร็จ';
        $data = DB::select("SELECT * FROM `position`");
        $num_positions = DB::table('position')->count();
        $report = array('positions' => $data, 'num_positions' => $num_positions, 'manager' => $manager, 'status' => $status);
        return response()->json($report);
    }

    public function search_star(){
        $users = DB::table('user')
            ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
            ->leftjoin('star', 'user.emp_id', '=', 'star.emp_id')
            ->select('employee.*', 'user.user_status', 'user.user_id','star.*')
            ->where('user.user_status', '<=', 2)
            ->get();
        foreach ($users as $key => $value){
            foreach ($value as $key2 => $value2){
                echo $key2 . ' => "' . $value2 . '"<br/>';
            }
            echo '<hr/>';
        }
        // $report = array('star' => $users);
        // return response()->json($report);
    }

    public function search_overall(){
        $position = DB::table('position')->get();
        $data=array();

        foreach ($position as $key => $value){
            $addPos = array();
            foreach ($value as $key2 => $value2){
                if ($key2 == 'pos_id'){
                    $numposition = DB::select("SELECT COUNT(emp_position) FROM `employee` WHERE `emp_position` = $value2");
                    foreach ($numposition[0] as $value3) {
                        array_push($addPos, $value2, $value3);
                    }
                } else if ($key2 == 'pos_name') {
                    array_push($addPos, $value2);
                }
            }
            array_push($data, $addPos);
        }

        foreach ($data as $key => $value){
            foreach ($value as $key2 => $value2){
                echo $key2 . " => " . $value2.'<br>';
            }
            echo '<hr>';
        }

        // $report = array('data' => $data);
        // return response()->json($report);
    }

    public function search_all_status(){
        $status=array(1, 2, 3, 4);
        $position = DB::table('user')
            ->select('user_status')
            ->get();
        $data=array();
        // print_r ($position);
        // echo "<hr>";
        $user_status = array('sta_1' => 0, 'sta_2' => 0, 'sta_3' => 0, 'sta_4' => 0);
        foreach ($position as $key => $value){
            foreach ($value as $key2 => $value2){
                switch ($value2) {
                    case '1':
                        $user_status['sta_1']++;
                        break;
                    case '2':
                        $user_status['sta_2']++;
                        break;
                    case '3':
                        $user_status['sta_3']++;
                        break;
                    case '4':
                        $user_status['sta_4']++;
                        break;
                    default:
                        echo 'default';
                }
            }
        }
        $data = $user_status;
        $report = array('data' => $data);
        return response()->json($report);
    }

    public function search_report(){
        return 'search_report OK';
    }

    public function search_subReport(){
        return 'search_subReport OK';
    }

    public function search_dowtime(){
        return 'search_dowtime OK';
    }

    public function search_strike(){
        return 'search_strike OK';
    }

    public function search_delegate(){
        return 'search_delegate OK';
    }

    public function search_notstrike(){
        return 'search_notstrike OK';
    }

    public function num_user (){
        $status = 200;
        $manager = 'อ่านข้อมูลสำเร็จ';
        $num_all = DB::table('user')->count();
        $num_allemp = DB::table('user')->where('user_status', '<=', 2)->count();
        $num_emp = DB::table('user')->where('user_status', 1)->count();
        $num_super = DB::table('user')->where('user_status', 2)->count();
        $num_manager = DB::table('user')->where('user_status', 3)->count();
        $num_admin = DB::table('user')->where('user_status', 4)->count();
        $report = array('num_all' => $num_all, 'num_allemp' => $num_allemp, 'num_employee' => $num_emp, 'num_superviser' => $num_super, 'num_manager' => $num_manager, 'num_admin' => $num_admin, 'manager' => $manager, 'status' => $status);
        return response()->json($report);
    }
}
