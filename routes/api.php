<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/routes_url', function()
{
    return 'สวัสดีชาวโลก2';
});

Route::any('/welcome', 'WelcomeController@welcome_laravel');
Route::any('/testdatabase', 'WelcomeController@testdatabase');
Route::any('/testdatabase2', function () {
    $data = DB::select("SELECT * FROM user");
    return response()->json($data);
});
Route::get('/check-connect',function(){
    if(DB::connection()->getDatabaseName())
    {
        return "Yes! successfully connected to the DB: " . DB::connection()->getDatabaseName();
    }else{
        return 'Connection False !!';
    }
});

Route::any('/search_user', 'SearchController@user');
Route::any('/search_oneuser', 'SearchController@oneuser');
Route::any('/search_manager', 'SearchController@manager');
Route::any('/alluser', 'SearchController@alluser');
Route::any('/all_register', 'SearchController@all_register');
Route::any('/positionsApi', 'SearchController@positions');

Route::any('/adduser', 'InsertController@adduser');
Route::any('/addmanager', 'InsertController@addmanager');
Route::any('/createUser', 'InsertController@createUser');
Route::post('createUserAPI', 'API\UserController@createUser');

Route::any('/delete_user', 'DeleteController@delete_user');

Route::any('/edit_user0', 'UpdateController@update_user');
Route::any('/update_register', 'UpdateController@update_register');
Route::any('/testUpload', 'UploadController@testUpload');
Route::any('/UploadImg', 'UploadController@uploadImg');

Route::any('/search_star', 'SearchController@search_star');
Route::any('/search_overall', 'SearchController@search_overall');
Route::any('/search_all_status', 'SearchController@search_all_status');
Route::any('/search_report', 'SearchController@search_report');
Route::any('/search_subReport', 'SearchController@search_subReport');
Route::any('/search_dowtime', 'SearchController@search_dowtime');
Route::any('/search_strike', 'SearchController@search_strike');
Route::any('/search_delegate', 'SearchController@search_delegate');
Route::any('/search_notstrike', 'SearchController@search_notstrike');
Route::any('/num_user', 'SearchController@num_user');


Route::group([
    'namespace' => 'login'
], function () {
    Route::any('/loginUser', 'LoginController@loginuser');
    Route::any('/loginAdmin', 'LoginController@loginadmin');
});

Route::group([
    'namespace' => 'upload'
], function () {
    Route::any('/uploadfile', 'UploadController@UploadFile');
});

//***************************************************************************************************** */
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');


Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::post('logout', 'API\UserController@logout');
    Route::any('/AddPositionsApi', 'API\UserController@AddPositionsApi');
    Route::any('/DeletePositionsApi', 'API\UserController@DeletePositionsApi');
    Route::any('/edit_user', 'UpdateController@update_user');

    Route::any('/newPassApi', 'API\UserController@newPassApi');
    // Route::any('/alluser', 'SearchController@alluser');
    Route::any('/add_star', 'UpdateController@add_star');
});
//***************************************************************************************************** */
